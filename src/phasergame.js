var mouseXYZ = new Phaser.Plugin.Isometric.Point3();
var character;
var characterDirection = 'east';
var characterAnimation = 'eastIdle';
var slashCount = 0;
var isJumping;
var speed = 100;
var cX, cY;

var eastSlash = ['eashSlash1', 'eastSlash2'];
var eastSlash1, eastSlash2;
var playingSlash;

var size;
var isoGroup;
var collisionGroup;

var game;
//416, 512
//32 x 32

game = new Phaser.Game(800, 600, Phaser.AUTO, 'test', null, false, true);
var BasicGame = function (game) {};
BasicGame.Boot = function (game) {};
BasicGame.Boot.prototype = {
  preload: function () {
      game.load.image('boarder','assets/border.png')
      game.load.image('cube','assets/cube.png')
      game.load.spritesheet('tiles','assets/map.png', 111, 128);
      //game.load.spritesheet('Adventurer','assets/Adventurer.png', 32, 32);
      game.load.atlas('adventuresprite', 'assets/adventuresprite.png', 'assets/adventuresprite.json');

      game.time.advancedTiming = true;

      game.plugins.add(new Phaser.Plugin.Isometric(game));
      game.world.setBounds(0, 0, 1000, 1000); //makes the size of the '3D' world
      game.physics.startSystem(Phaser.Plugin.Isometric.ISOARCADE);
      game.iso.anchor.setTo(0.5, 0.5);
  },
  create: function () {
      isoGroup = game.add.group();
      collisionGroup = game.add.group();

      game.physics.isoArcade.gravity.setTo(0, 0, -500);
      // create the floor tiles
      var floorTile;
      let block;
      let thePosition;
      let theBlock;
      size = 38;
      for (var integralX = size; integralX <= game.physics.isoArcade.bounds.frontX - size; integralX += size) {
          for (var integralY = size; integralY <= game.physics.isoArcade.bounds.frontY - size; integralY += size) {
              floorTile = game.add.isoSprite(integralX, integralY, Math.random() * 10, 'boarder', 0, isoGroup);
              floorTile.anchor.set(.5, .5);
              if (Math.floor(Math.random()*10)==2 && !block) {
                block = true;
                cube = game.add.isoSprite(integralX, integralY, 0, 'cube', 0, collisionGroup);
                cube.anchor.set(.5);
                game.physics.isoArcade.enable(cube);
                cube.body.collideWorldBounds = true;
                //theBlock.body.immovable = true;
                                // Add a full bounce on the x and y axes, and a bit on the z axis. 
                cube.body.bounce.set(1, 1, 0.2);

                // Add some X and Y drag to make cubes slow down after being pushed.
                cube.body.drag.set(100, 100, 0);
              }
          }
      }
      character = game.add.isoSprite(cX, cY, 0, 'adventuresprite',0,collisionGroup);
      character.scale.setTo(3,3);
      character.anchor.set(.5);
      var west = 81;
      var westAnimations = [];
      /*
      for (let i = 7; i <= 0; i--) { // make function
        westAnimations.push(west + i);
      }
      */
      for (let i = 0; i <= 7; i++) {
        westAnimations.push(west+i);
      }
      character.animations.add('west',westAnimations,12,false);// use loop lol

      var westIdle = 69;
      var westIdleAnimations = [];
      for (let i = 0; i <= 12; i++) {
        westIdleAnimations.push(westIdle+i);
      }
      character.animations.add('westIdle',westIdleAnimations,6,false);

      var east = 13;
      var eastAnimations = [];
      for (let i = 0; i <= 7; i++) {
        eastAnimations.push(east+i);
      }

      character.animations.add('east', eastAnimations,12,false);

      var eastIdleAnimations = [];
      for (let i = 0; i <= 12; i++) {
        eastIdleAnimations.push(i);
      }
      character.animations.add('eastIdle',eastIdleAnimations,6,false);

      var eastJump = 13;
      var eastJumpAnimations = [];
      for (let i = 0; i <= 8; i++) {
        eastJumpAnimations.push(eastJump + i);
      }
      character.animations.add('eastJump',eastJumpAnimations,12,false);
      //30, 41
      eastSlash1Items = [];
      eastSlash2Items = [];
      for (let i = 0; i <= 9; i++) {
        eastSlash1Items.push(30 + i);
        eastSlash2Items.push(41 + i);
      }
      eastSlash1 = character.animations.add('eastSlash1',eastSlash1Items,12,false)
      eastSlash2 = character.animations.add('eastSlash2',eastSlash2Items,12,false)

      character.animations.play(characterAnimation)

      // enable physics on the player
      game.physics.isoArcade.enable(character);
      character.body.collideWorldBounds = true;

      game.camera.follow(character);

      //game.input.mouse.capture = true;
      //game.input.onDown.add(doSomething, this);
  },
  update: function() {
      character.body.velocity.x = 0;
      character.body.velocity.y = 0;
      
      /*
      if (characterDirection == 'east') {
          characterAnimation = 'east';
      } else if (characterDirection == 'west') {
          characterAnimation = 'west';
      }
      if (game.input.keyboard.isDown(Phaser.Keyboard.W)) {
          //characterSprite.setVelocityY(-characterSpeed);
          character.body.velocity.y = -speed;
      }
      if (game.input.keyboard.isDown(Phaser.Keyboard.S)) {
          character.body.velocity.y = speed;
      }
      if (game.input.keyboard.isDown(Phaser.Keyboard.A)) {
          character.body.velocity.x = -speed;
          characterDirection = 'west'; characterAnimation = 'west';
      }
      if (game.input.keyboard.isDown(Phaser.Keyboard.D)) {
          character.body.velocity.x = speed;
          characterDirection = 'east'; characterAnimation = 'east';
      }
      if (game.input.keyboard.justPressed(Phaser.Keyboard.SPACEBAR)) {
        if (character.body.onFloor()) {
          character.body.velocity.z = speed*2;
          if (characterDirection == 'east') {
            characterAnimation = 'eastJump';
          } else if (characterDirection == 'west') {
            characterAnimation = 'westJump';
          }
          character.animations.play(characterAnimation);
        }
      }
      if (!game.input.keyboard.isDown(Phaser.Keyboard.W) && !game.input.keyboard.isDown(Phaser.Keyboard.A) &&
        !game.input.keyboard.isDown(Phaser.Keyboard.S) && !game.input.keyboard.isDown(Phaser.Keyboard.D)){
          //character.animations.stop();
          //character.body.velocity.x = 0;
          if (character.body.onFloor() || character.body.velocity.z < 0) {
            if (characterDirection == 'east') {
              characterAnimation = 'eastIdle';
            } else if (characterDirection == 'west') {
              characterAnimation = 'westIdle';
            }
         }
      }*/
      //console.log(character.animations.currentAnim.name)
      if (game.input.activePointer.isDown) {
        characterAnimation = eastSlash[Math.floor(Math.random() * eastSlash.length)]
        characterAnimation.onComplete.addOnce

        /*
        if (!eastSlash1.isPlaying && !eastSlash2.isPlaying) {
          if (!playingSlash) {
            playingSlash = true
            characterAnimation = eastSlash[Math.floor(Math.random() * eastSlash.length)]
            character.animations.play(characterAnimation)
            console.log('lol')
          } else if (playingSlash) {
            playingSlash = false
          }
        }
        slashCount ++;
      //}
      } else {
          characterAnimation = characterDirection+'Idle'
          slashCount = 0;*/
      } else {//just for now
        characterAnimation = characterDirection+'Idle'
      }
   //character.animations.play(characterAnimation)
      character.animations.play(characterAnimation)
      game.physics.isoArcade.collide(collisionGroup);
      game.iso.topologicalSort(collisionGroup);
  },
  render: function() {
      game.debug.octree(game.physics.isoArcade.octree);
      game.debug.body(character);
  }
}
game.state.add('Boot', BasicGame.Boot);
game.state.start('Boot');

function slash() {

}
