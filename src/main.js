let cursorPos;
var character;
var WKey, AKey, SKey, DKey;
var size;
var isoGroup;
var collisionGroup;
var potion;
var colors = [0xF5F975, 0x3BD269, 0x48B2C5];//sand, water and grass
var water = [];
var ground;
var groundTiles = [];

let moveTable = {};
let slashTable = {};
let isSlashing;
let isRunning;
let direction = 'east';
let characterAnim = 'eastIdle';//make currentAnim to character
let speed = 100;
let isChanging;

let currentSelected;
//416, 512
//32 x 32

var game = new Phaser.Game(1000, 600, Phaser.AUTO, 'test', null, false, true);
var BasicGame = function (game) {};
BasicGame.Boot = function (game) {};
BasicGame.Boot.prototype = {
  addAnimation: function(name, frameData, frameRate, looping) {
    let animation = [];
    let starting = frameData[0]
    let ending = frameData[1]

    for (let i = 0; i <= ending; i++) {
      animation.push(starting + i);
    }
    try {
      animation = character.animations.add(name, animation, frameRate, looping);
      for (let suffix = 0; suffix <= 1; suffix++) {
        if (name == 'east' + 'Slash' + suffix ||  name == 'west' + 'Slash' + suffix) {
          slashTable[name] = animation;
        } else {
          moveTable[name] = animation;
        }
      }
    } catch(exception) {
        throw new Error(exception);
    }
    return animation;
  },

  preload: function () { //load assets to memory
      game.load.image('border','assets/border.png')
      game.load.image('cube','assets/cube.png')
      game.load.spritesheet('tiles','assets/map.png', 111, 128);
      game.load.atlas('adventuresprite', 'assets/adventuresprite.png', 'assets/adventuresprite.json');
      game.load.atlas('items1', 'assets/items-1.png', 'assets/items-1.json');

      game.time.advancedTiming = true;
      game.plugins.add(new Phaser.Plugin.Isometric(game));
      game.world.setBounds(0, 0, 2048, 2048); //makes the size of the '3D' world
      game.physics.startSystem(Phaser.Plugin.Isometric.ISOARCADE);
      game.iso.anchor.setTo(0.5, 0.5);
  },

  slash: function() {//slashing / hit handler
    if (!isSlashing) {
      isSlashing = true
      characterAnim = slashTable[direction + "Slash" + Math.floor(Math.random() * 2)]; // instead of character anim variable, could just replace currentAnim
      character.animations.play(characterAnim.name)
      speed = 50;

      characterAnim.onComplete.addOnce(function() {
        isSlashing = false
        speed = 100;
      }, this);
    }
  },

  jump: function() { // jumping
    if (!isSlashing && character.body.onFloor()) {
      character.body.velocity.z = speed*2;
    }
  },

  create: function () { // this function by default runs once
      isoGroup = game.add.group();
      ground = game.add.group();
      collisionGroup = game.add.group();
      //water = game.add.group();

      game.physics.isoArcade.gravity.setTo(0, 0, -500);

      let floorTile;
      let isGround;
      let block;
      let thePosition;
      let theBlock;
      let cX, cY;
      size = 38;

      //map generation
      for (var integralX = size; integralX <= game.physics.isoArcade.bounds.frontX - size; integralX += size) {
          for (var integralY = size; integralY <= game.physics.isoArcade.bounds.frontY - size; integralY += size) {
              let isometricZ;
              let tintSelection;
              if ((integralX <= 128 || integralY <= 128) || (integralX >= 128*6.7 || integralY >= 128*6.7)) { // builds water around
                tintSelection = colors[2];
                floorTile = game.add.isoSprite(integralX, integralY, 0, 'border', 0, collisionGroup);
                water.push(floorTile);
                floorTile.initialZ = isometricZ;
                isometricZ = Math.random() * -2;
                isGround = false;
              //} else if ((integralX > 128 && integralX < 128 + 38) || (integralY > 128 && integralY < 128 + 38)) {
              } else if ((integralX <= 128 + 38 || integralY >= 128*6.3) || (integralY <= 128 + 38 || integralX >= 128*6.3)) {
              //} else if ((integralX < 128 + 38 || integralX > 128 * 7) || (integralY < 128 + 38 || integralY > 128 * 7)) {
                tintSelection = colors[0]
                floorTile = game.add.isoSprite(integralX, integralY, 0, 'border', 0, ground);
                isometricZ = Math.random() * -5;
                isGround = true;
              } else {
                tintSelection = colors[1]
                floorTile = game.add.isoSprite(integralX, integralY, 0, 'border', 0, ground);
                isoGroup.remove(floorTile);
                ground.add(floorTile);
                isGround = true;
                isometricZ = Math.random() * 6;
                groundTiles.push(floorTile);
              }
              floorTile.anchor.set(.5, .5);
              floorTile.tint = tintSelection // hex color value
              floorTile.isoZ = isometricZ;
              floorTile.initialZ = isometricZ;
              if (!isGround) {
                game.physics.isoArcade.enable(floorTile);
                floorTile.body.collideWorldBounds = true;
                floorTile.body.immovable = true;
              }
              if (Math.floor(Math.random()*30)==2 && isGround) {
                block = true;
                potion = game.add.isoSprite(integralX, integralY, 0, 'items1', Math.floor(Math.random() * 10) + 20, collisionGroup);
                potion.scale.setTo(.6,.6)
                potion.anchor.set(.5);
                game.physics.isoArcade.enable(potion);
                potion.body.collideWorldBounds = true; 
                potion.body.bounce.set(1, 1, 0.2);
                potion.body.drag.set(100, 100, 0);
              }
          }
      }

      let groundSpawn = groundTiles[Math.floor(Math.random() * groundTiles.length)];
      character = game.add.isoSprite(groundSpawn.isoX, groundSpawn.isoY, 0, 'adventuresprite', 0, collisionGroup);
      character.scale.setTo(3,3);
      character.anchor.set(.5);

      this.addAnimation('westIdle', [68, 12], 12, false);
      this.addAnimation('westWalk', [81, 7], 12, false);

      this.addAnimation('westSlash0', [97, 9], 12, false);
      this.addAnimation('westSlash1', [107, 9], 12, false);

      this.addAnimation('eastIdle', [0, 12], 12, false);
      this.addAnimation('eastWalk', [13, 7], 12, false);

      this.addAnimation('eastSlash0', [30, 9], 12, false);
      this.addAnimation('eastSlash1', [41, 9], 12, false);

      characterAnim = moveTable[characterAnim];
      character.animations.play(characterAnim.name);

      // enable physics on the player
      game.physics.isoArcade.enable(character);
      character.body.collideWorldBounds = true;

      //game.physics.isoArcade.enable(isoGroup);

      game.camera.follow(character);

      WKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
      AKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
      SKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
      DKey = game.input.keyboard.addKey(Phaser.Keyboard.D);

      SPACEKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
      game.input.keyboard.addKeyCapture(SPACEKey) // stop key from moving webpage

      game.input.onDown.add(this.slash, this);
      SPACEKey.onDown.add(this.jump, this);

      cursorPos = new Phaser.Plugin.Isometric.Point3();

  },
  update: function() { // constantly runs
      character.body.velocity.x = 0;
      character.body.velocity.y = 0;

      //movement & animation handling
      if (WKey.isDown) {
        character.body.velocity.y = -speed;
      }
      if (SKey.isDown) {
        character.body.velocity.y = speed;
      }
      if (AKey.isDown) {
        character.body.velocity.x = -speed;
        direction = 'west';
      }
      if (DKey.isDown) {
        character.body.velocity.x = speed;
        direction = 'east';
      }
      if (!WKey.isDown && !AKey.isDown && !SKey.isDown && !DKey.isDown) { 
        if (!isSlashing) {
          characterAnim = moveTable[direction + "Idle"];
          isRunning = false;
        }
      } else {
        isRunning = true;
      }
      if (!isSlashing) {
        character.animations.play(characterAnim.name)
        if (isRunning) {
          characterAnim = moveTable[direction + 'Walk'];
        }
      }
      
      game.physics.isoArcade.collide(collisionGroup);
      game.iso.topologicalSort(collisionGroup);

      // cool wave effect using trig functions
      water.forEach(function(tile) {
        tile.isoZ =
        tile.initialZ +
        (-2 * Math.sin((this.game.time.now + (tile.isoX * 7)) * 0.004))
        + (-1 * Math.sin((this.game.time.now + (tile.isoY * 8)) * 0.005));
       });

      game.debug.text("FPS: " + game.time.fps || '--', 2, 14, "#FFFFFF");

      //handles painting
      game.iso.unproject(game.input.activePointer.position, cursorPos);

      ground.forEach(function (tile) {
          var inBounds = tile.isoBounds.containsXY(cursorPos.x, cursorPos.y);

          if (!tile.selected && inBounds) {
              tile.original = tile.tint;
              tile.selected = true;
              tile.tint = 0xFFFFFF;
              game.add.tween(tile).to({ isoZ: tile.initialZ + 5 }, 200, Phaser.Easing.Quadratic.InOut, true);
          } else if (tile.selected && !inBounds) {
              tile.selected = false;
              tile.tint = tile.original || 0xFFFFFF;
              game.add.tween(tile).to({ isoZ: tile.initialZ }, 200, Phaser.Easing.Quadratic.InOut, true);
        }
      });
  },
  render: function() {
    //game.debug.octree(game.physics.isoArcade.octree);  
  }
}

game.state.add('Boot', BasicGame.Boot);
game.state.start('Boot');